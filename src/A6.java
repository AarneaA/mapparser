import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.imageio.ImageIO;

// receives skeletons, joins them on a int[][], creates an image again, sends to UI
public class A6 implements Runnable{

	private Thread thread;
	protected BlockingQueue<int[][]> queue;
	private int outputIterator;
	
	public A6() { this(new LinkedBlockingQueue<int[][]>()); }
	
	public A6(LinkedBlockingQueue<int[][]> linkedBlockingQueue) {
		this.queue = linkedBlockingQueue;
		thread = new Thread(this, this.toString());
		outputIterator = 1;
	}
	
//	TODO: implement this
	public void process(int[][] msg){
		System.out.println("Actor6 start. Iterator: " + outputIterator);
//		make image out of int[][] and send to UI
		BufferedImage image = new BufferedImage(msg[0].length, msg.length, BufferedImage.TYPE_INT_RGB);
//		go over all objects again and transpose them onto the new image resolutions.
//		Might have to fill areas inbetween 
		for(int rownum = 0; rownum < msg.length; rownum++/*int[] row: parsedObjects.get(index)*/){
			for(int colnum = 0; colnum < msg[rownum].length; colnum++/*int col: row*/){
				image.setRGB(colnum, rownum, (msg[rownum][colnum] == 1)? 5000000:0);
			}
		}
		
		
		FileOutputStream outs;
			try {
				outs = new FileOutputStream(new File("output_" + outputIterator + ".png"));
//				outputIterator++;
				ImageIO.write(image, "png", outs);
				System.out.println("Actor6 end");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally{
				outputIterator++;
			}
			
	};
	
	public void start(){
		thread.start();
	}
	
	public void send(int[][] msg) {
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		try {
			while(true){
			int[][] message = queue.take();
			process(message);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
