import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

// receives symbol as int[][] and creates a DMap, 
public class A3 implements Runnable{

	private Thread thread;
	protected BlockingQueue<int[][]> queue;
	
	public A3() { this(new LinkedBlockingQueue<int[][]>()); }
	
	public A3(LinkedBlockingQueue<int[][]> linkedBlockingQueue) {
		this.queue = linkedBlockingQueue;
		thread = new Thread(this, this.toString());
	}
	
	private void messageOut(int[][] msg, A4 actor4){
		actor4.send(msg);
	}
	
//	TODO: implement this
//	int[][] into dmap, send to A4 to skeletonize A5 as progress
	public void process(int[][] object){
		System.out.println("Actor3 start");
		A4 actor4 = new A4();
		actor4.start();
		
//		TODO: loop over enlarged object squares, creating a distancemap from edges (squares with neighbouring (4-connectivity)
//		empty square have distance 0, full squares have min neighbour +1)
		int[][]distanceMap = new int[object.length][object[0].length];
		for(int row = 0; row < object.length; row++){
			for(int col = 0; col < object[0].length; col++){
//				Find all edges and give distances of 0 (1s with neighbour 0)
				if(object[row][col] == 1){
					distanceMap[row][col] = -2;
					Stack<int[]> neighboursToCheck = new Stack();
					int[] n1 = {row - 1, col};
					int[] n2 = {row, col - 1};
					int[] n3 = {row, col + 1};
					int[] n4 = {row + 1, col};
					if(row > 0)
						neighboursToCheck.push(n1);	
					if(row < object.length - 1)
						neighboursToCheck.push(n4);
					if(col > 0)
						neighboursToCheck.push(n2);
					if(col < object[0].length - 1)
						neighboursToCheck.push(n3);
//					if any neighbor is 0, give value of 0
					innerWhileLoop:
					while(!neighboursToCheck.isEmpty()){
						int[] checking = neighboursToCheck.pop();
//						System.out.println(checking[0] + ", " + checking[1]);
//						System.out.println(object.length + "; " + object[0].length);
						if(object[checking[0]][checking[1]] == 0){
							distanceMap[row][col] = 0;
							break innerWhileLoop;
						}
					}
				}
				else{
					distanceMap[row][col] = -1;
				}
//				Check 4-connected neighbours for contents etc. and make distancemap
				
			}
		}
//		return distanceMap;
		for(int row = 0; row < object.length; row++){
			for(int col = 0; col < object[0].length; col++){
//				Find all edges and give distances of 0 (1s with neighbour 0)
				if(distanceMap[row][col] != -1 && distanceMap[row][col] != 0){
					Stack<int[]> neighboursToCheck = new Stack();
					ArrayList<Integer> neighbourValues = new ArrayList();
					int[] n1 = {row - 1, col};
					int[] n2 = {row, col - 1};
					int[] n3 = {row, col + 1};
					int[] n4 = {row + 1, col};
					if(row > 0)
						neighboursToCheck.push(n1);	
					if(row < object.length - 1)
						neighboursToCheck.push(n4);
					if(col > 0)
						neighboursToCheck.push(n2);
					if(col < object[0].length - 1)
						neighboursToCheck.push(n3);
//					if any neighbor is 0, give value of 0
					while(!neighboursToCheck.isEmpty()){
						int[] checking = neighboursToCheck.pop();
						neighbourValues.add(object[checking[0]][checking[1]]);
					}
//					Find min neighbourvalue that is not -1
					int currentMin = Integer.MAX_VALUE;
					for(int value: neighbourValues){
						if (value < currentMin && value != -1)
							currentMin = value;
					}
					distanceMap[row][col] = currentMin + 1;
				}
			}
		}
//		return distanceMap;
//		send to A4
		messageOut(distanceMap, actor4);
		System.out.println("Actor3 end");
	}
	
	public void start(){
		thread.start();
	}
	
	public void send(int[][] msg) {
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		try {
			while(true){
			int[][] message = queue.take();
			process(message);
//			Send messages to 2 other actors
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
