import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ImageBack extends JPanel {

	public ImageBack()
    {
        ImageIcon image = new ImageIcon("SkeletonInput.png");
        JLabel imageLabel = new JLabel(image); 
        imageLabel.setBounds(10, 10, 800, 100);
        imageLabel.setVisible(true);
        add(imageLabel);
        }
	
}
