import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

// receives progress message, processes as which kind of progress it is (depending on the actor sending it)
public class AUI extends JFrame implements Runnable{

	private Thread thread;
	protected BlockingQueue<String> queue;
	private int a1Progress = 0;
	private int a2Progress = 0;
	private int a3Progress = 0;
	private int a4Progress = 0;
	private int a6Progress = 0;
	private int[] msgs;
	
	public void redraw(){
		this.setVisible(false);
		AUI newFrame = new AUI("3_4.png");
		newFrame.setVisible(true);
	}
	
	public AUI() { this(new LinkedBlockingQueue<String>()); }
	
	public AUI(String imageName) { 
		this(new LinkedBlockingQueue<String>());
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        ImageIcon image = new ImageIcon(imageName);
        JLabel imageLabel = new JLabel(image); 
        panel.add(imageLabel);
        JLabel a1progressLabel = new JLabel("A1 progress: " + a1Progress + "  ");
        JLabel a2progressLabel = new JLabel("A2 progress: " + a2Progress + "  ");
        JLabel a3progressLabel = new JLabel("A3 progress: " + a3Progress + "  ");
        JLabel a4progressLabel = new JLabel("A4 progress: " + a4Progress + "  ");
        JLabel a6progressLabel = new JLabel("A6 progress: " + a6Progress + "  ");
        panel.add(a1progressLabel);
        panel.add(a2progressLabel);
        panel.add(a3progressLabel);
        panel.add(a4progressLabel);
        panel.add(a6progressLabel);
        this.add(panel);
		}
	
	public AUI(LinkedBlockingQueue<String> linkedBlockingQueue) {
		this.queue = linkedBlockingQueue;
		thread = new Thread(this, this.toString());
	}
	
//	TODO: implement this
	public void process(String msg){
		if(msg.equals("A1")){
			a1Progress++;
		}
		else if(msg.equals("A2")){
			a2Progress++;
		}
		else if(msg.equals("A3")){
			a3Progress++;
		}
		else if(msg.equals("A4")){
			a4Progress++;
		}
		else if(msg.equals("A6")){
			a6Progress++;
		}
		else{
//			Must be filename, try to display
		}
	};
	
	public void start(){
		thread.start();
	}
	
	public void send(String msg) {
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		try {
			while(true){
			String message = queue.take();
			process(message);
//			Send messages to 2 other actors
			msgs = new int[4];
			msgs[0] = a1Progress;
			msgs[1] = a2Progress;
			msgs[2] = a3Progress;
			msgs[3] = a4Progress;
//			send msgs to UI Actor
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
