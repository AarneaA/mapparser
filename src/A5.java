import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

// receives progress message, processes as which kind of progress it is (depending on the actor sending it)
public class A5 implements Runnable{

	private Thread thread;
	protected BlockingQueue<String> queue;
	private int a1Progress = 0;
	private int a2Progress = 0;
	private int a3Progress = 0;
	private int a4Progress = 0;
	private int[] msgs;
	
	public A5() { this(new LinkedBlockingQueue<String>()); }
	
	public A5(LinkedBlockingQueue<String> linkedBlockingQueue) {
		this.queue = linkedBlockingQueue;
		thread = new Thread(this, this.toString());
	}
	
//	TODO: implement this
	public void process(String msg){
		if(msg.equals("A1")){
			a1Progress++;
		}
		else if(msg.equals("A2")){
			a2Progress++;
		}
		else if(msg.equals("A3")){
			a3Progress++;
		}
		else if(msg.equals("A4")){
			a4Progress++;
		}
	};
	
	public void start(){
		thread.start();
	}
	
	public void send(String msg) {
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		try {
			while(true){
			String message = queue.take();
			process(message);
//			Send messages to 2 other actors
			msgs = new int[4];
			msgs[0] = a1Progress;
			msgs[1] = a2Progress;
			msgs[2] = a3Progress;
			msgs[3] = a4Progress;
//			send msgs to UI Actor
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
