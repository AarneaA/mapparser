import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class concurrentSkeletonization extends JFrame {
		
//	    public concurrentSkeletonization()
//	    {
//
//	        ImageIcon image = new ImageIcon("3_4.png");
//	        JLabel imageLabel = new JLabel(image); 
////	        imageLabel.setBounds(10, 10, 800, 100);
////	        imageLabel.setVisible(true);
//	        add(imageLabel);
//	        }
	
	
	public static void main(String[] args){
		
//		concurrentSkeletonization swingClient = new concurrentSkeletonization();
//		swingClient.setSize(800,400);
//		swingClient.setVisible(true);
//		System.out.println(swingClient.getSize().toString());
		
//		Send int[][] image to A1
		A1 actor1 = new A1();
		AUI actorUI = new AUI("3_4.png");
		actorUI.start();
		actorUI.setSize(800,400);
		actorUI.setVisible(true);
//		actorUI.send("3_4.png");		
		actor1.start();
		actor1.send("3_4.png");
		
		while(true){
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			actorUI.redraw();
		}
//		=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
		
//		first ask input file name in console, handle with promise queue if exists vs if doesn't follow lecture examples
//		if doesnt exist either exit or send back to beginning
		
//		if does exist, start Swing UI with input image and 0 progress
//		increase contrast DFS for elements, start sending them to A2
		
//		A2 create distancemaps, send them to A3
//		send progresses to A4
//			send skeletons to A1
		
//		A3 skeletonize the objects, send back to actor 2
//		send progresses to A4
		
//		A4 change the progress integer, send to UI
		
	}
	
}
