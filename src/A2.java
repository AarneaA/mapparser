import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.imageio.ImageIO;

// receives symbol as int[] and creates a int[][] of it,
//TODO: Change to message with int[] symbol and image name tag
// and also to UI?
public class A2 implements Runnable{

	private Thread thread;
	protected BlockingQueue<int[]> queue;
	private int iterator;
	
	public A2() { this(new LinkedBlockingQueue<int[]>()); }
	
	public A2(LinkedBlockingQueue<int[]> linkedBlockingQueue) {
		this.queue = linkedBlockingQueue;
		thread = new Thread(this, this.toString());
		iterator = 0;
	}
	
	private void messageOut(int[][] msg, A3 actor3){
		actor3.send(msg);
		System.out.println(iterator);
	}
	
//	TODO: implement this
//	Turn int[] into int[][]
	public void process(int[] currentElement, BufferedImage img){
		System.out.println("Actor2 start");
		A3 actor3 = new A3();
		actor3.start();
		iterator++;
		
		//System.out.println("Creating new object");
//		Array of pixel indexes -> boolean[][] 
		int width = img.getWidth();
		int maxX = 0, maxY = 0;
		int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
		int rownum, colnum;
//		Find min and max x and y
		if(currentElement.length > 0){
			//System.out.println(currentElement.length);
			for(int index: currentElement){
				rownum = (int) Math.floor(index/img.getWidth()); 		// Y
		        colnum = index-(rownum*img.getWidth());					// X
				if(colnum < minX) minX = colnum;
				if(rownum < minY) minY = rownum;
				if(colnum > maxX) maxX = colnum;
				if(rownum > maxY) maxY = rownum;
			}
		
			//System.out.println("" + maxY +" "+ minY + "\t" + maxX + " " + minX);
			int[][] object = new int[maxY-minY+1][maxX-minX+1];
	//		Populate obect
			for(int index: currentElement){
				rownum = (int) Math.floor(index/img.getWidth()) /*- minY*/; 		// Y
		        colnum = index-(rownum*img.getWidth()) - minX;						// X
		        rownum -= minY;
	//	        //System.out.println("x: " + colnum);
	//	        //System.out.println("minX: " + minX);
	//	        //System.out.println("diff: " + (colnum-minX));
	//	        //System.out.println("Width: " + object[0].length);
		        object[rownum][colnum] = 1;
			}
			messageOut(object, actor3);
			System.out.println("Actor2 end");
//			return object;
		}
//		return null;
	}
	
	public void start(){
		thread.start();
	}
	
	public void send(int[] msg) {
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		try {
			File img1 = new File("3_4.png");
			BufferedImage img = ImageIO.read(img1);
			while(true){
			int[] message = queue.take();
			
			process(message, img);
//			Send messages to 2 other actors
			}
		} catch (InterruptedException | IOException e) {
			Thread.currentThread().interrupt();
		}
	}

}
