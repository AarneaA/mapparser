import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import javax.imageio.ImageIO;

public class main {
	
	public void edgeDetect(String inputFileName, String outputFileName){
		File img1 = new File(inputFileName);
	    BufferedImage img;
		try {
			img = ImageIO.read(img1);
		    int[] colors = new int[img.getWidth() * img.getHeight()];
		    int[][] rCross = new int[img.getHeight()][img.getWidth()];
		    int[] rCrossSingleArray = new int[img.getWidth() * img.getHeight()];
		    int rCrossModifier = 1;
		    img.getRGB(0, 0, img.getWidth(), img.getHeight(), colors, 0, img.getWidth());
		    
	//	    rgb values to image
		    BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
		    BufferedImage imageOut = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB); 
		       for (int x = 0; x < img.getWidth() * img.getHeight(); x++) {
		          int rgb = colors[x];
		          Color color = new Color(colors[x]);
	//	          ////System.out.println("{ " + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + " }");
		          int rownum = (int) Math.floor(x/img.getWidth());
		          int colnum = x-(rownum*img.getWidth());
		          image.setRGB(colnum, rownum, rgb);
	//	          For a line that has a full line below it, we can calculate Roberts Cross value.
	//	          Before calculation, make grayscale.
		          if(rownum > 1 && colnum < img.getWidth() - 1) {
		        	  Color p1C = new Color(image.getRGB(colnum, rownum - 2));
		        	  float p1 = (p1C.getRed() + p1C.getGreen() + p1C.getBlue()) / 3; 
	//	        	  ////System.out.println(colnum + 1 + " / " + img.getWidth());
		        	  Color p2C = new Color(image.getRGB(colnum + 1, rownum - 2));
		        	  float p2 = (p2C.getRed() + p2C.getGreen() + p2C.getBlue()) / 3;
		        	  Color p3C = new Color(image.getRGB(colnum, rownum - 1));
		        	  float p3 = (p3C.getRed() + p3C.getGreen() + p3C.getBlue()) / 3;
		        	  Color p4C = new Color(image.getRGB(colnum + 1, rownum - 1));
		        	  float p4 = (p4C.getRed() + p4C.getGreen() + p4C.getBlue()) / 3;
		        	  Color rCrossColor = new Color (Math.round(Math.abs(p1 - p4) + Math.abs(p2 - p3)));
		        	  int rCrossValue = Math.round(Math.abs(p1 - p4) + Math.abs(p2 - p3));
//		        	  if(rCrossValue < 40) rCrossValue = 0;
//		        	  else rCrossValue = 100000000;
	//	        	  ////System.out.println("Roberts Cross value: " + rCrossColor.getRed() + ", " + rCrossColor.getGreen() + ", " + rCrossColor.getBlue());
		        	  rCross[rownum - 2][colnum] = rCrossValue;
	//	        	  Currently overwriting Roberts Cross onto output.
		        	  imageOut.setRGB(colnum, rownum-2, rCrossValue);
	//	       		TODO: When reaches the end, process last 2 rows aswell.
		          }
		       }
		      FileOutputStream outs = new FileOutputStream(new File(outputFileName));
	          ImageIO.write(imageOut, "png", outs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void edgeDetectSobel(String inputFileName, String outputFileName){
		File img1 = new File(inputFileName);
	    BufferedImage img;
		try {
			img = ImageIO.read(img1);
		    int[] colors = new int[img.getWidth() * img.getHeight()];
		    int[][] sobel = new int[img.getHeight()][img.getWidth()];
		    img.getRGB(0, 0, img.getWidth(), img.getHeight(), colors, 0, img.getWidth());
		    
	//	    rgb values to image
		    BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
		    BufferedImage imageOut = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
		       for (int x = 0; x < img.getWidth() * img.getHeight(); x++) {
		          int rgb = colors[x];
		          int rownum = (int) Math.floor(x/img.getWidth());
		          int colnum = x-(rownum*img.getWidth());
		          image.setRGB(colnum, rownum, rgb);
	//	          For a line that has a full line below it, we can calculate Roberts Cross value.
	//	          Before calculation, make grayscale.
		          if(rownum > 1 && colnum < img.getWidth() - 2) {
		        	  
		        	  Color p1C = new Color(image.getRGB(colnum, rownum - 2));
		        	  float p1 = (p1C.getRed() + p1C.getGreen() + p1C.getBlue()) / 3; 
		        	  Color p2C = new Color(image.getRGB(colnum + 1, rownum - 2));
		        	  float p2 = (p2C.getRed() + p2C.getGreen() + p2C.getBlue()) / 3;
		        	  Color p3C = new Color(image.getRGB(colnum + 2, rownum - 2));
		        	  float p3 = (p3C.getRed() + p3C.getGreen() + p3C.getBlue()) / 3;
		        	  
		        	  Color p4C = new Color(image.getRGB(colnum, rownum - 1));
		        	  float p4 = (p4C.getRed() + p4C.getGreen() + p4C.getBlue()) / 3;
//		        	  middle square never used
//		        	  Color p5C = new Color(image.getRGB(colnum + 1, rownum - 2));
//		        	  float p5 = (p5C.getRed() + p5C.getGreen() + p5C.getBlue()) / 3;
		        	  Color p6C = new Color(image.getRGB(colnum + 2, rownum - 1));
		        	  float p6 = (p6C.getRed() + p6C.getGreen() + p6C.getBlue()) / 3;
		        	  
		        	  Color p7C = new Color(image.getRGB(colnum, rownum));
		        	  float p7 = (p7C.getRed() + p7C.getGreen() + p7C.getBlue()) / 3;
		        	  Color p8C = new Color(image.getRGB(colnum + 1, rownum));
		        	  float p8 = (p8C.getRed() + p8C.getGreen() + p8C.getBlue()) / 3;
		        	  Color p9C = new Color(image.getRGB(colnum + 2, rownum));
		        	  float p9 = (p9C.getRed() + p9C.getGreen() + p9C.getBlue()) / 3;
		        	  
//		        	  float Gx = p3 + (2 * p6) + p9 - p1 - (2 * p4) - p7;
//		        	  float Gy = p1 + (2 * p2) + p3 - p7 - (2 * p8) - p9;
		        	  
//		        	  int sobelValue = Math.round(Math.abs(Gx) + Math.abs(Gy));
		        	  
		        	  int sobelValue = Math.round(Math.abs((p1+2*p2+3) - (p7+2*p8+p9)) + Math.abs((p3+2*p6+p9) - (p1+2*p4+p7)));
		        	  
//		        	  if(sobelValue < 750) sobelValue = 0;
//		        	  else sobelValue = 100000000;
		        	  sobel[rownum - 2][colnum] = sobelValue;
		        	  imageOut.setRGB(colnum, rownum-2, sobelValue);
	//	       		TODO: When reaches the end, process last 2 rows aswell.
		          }
		       }
		      FileOutputStream outs = new FileOutputStream(new File(outputFileName));
	          ImageIO.write(imageOut, "png", outs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void contrastIncrease(String inputFileName, String outputFileName, float d){
		RescaleOp op = new RescaleOp(d,1,null);
		File img1 = new File(inputFileName);
		BufferedImage img;
		try {
			img = ImageIO.read(img1);
//		BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
		op.filter(img, img);
		FileOutputStream outs = new FileOutputStream(new File(outputFileName));
        ImageIO.write(img, "png", outs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void grayScale(String inputFileName, String outputFileName){

		File img1 = new File(inputFileName);
	    BufferedImage img;
		try {
			img = ImageIO.read(img1);
		    int[] colors = new int[img.getWidth() * img.getHeight()];
		    img.getRGB(0, 0, img.getWidth(), img.getHeight(), colors, 0, img.getWidth());
		    
	//	    rgb values to image
		    BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_GRAY); 
		       for (int x = 0; x < img.getWidth() * img.getHeight(); x++) {
		          int rgb = colors[x];
		          int rownum = (int) Math.floor(x/img.getWidth());
		          int colnum = x-(rownum*img.getWidth());
		          image.setRGB(colnum, rownum, rgb);
		       }
		      FileOutputStream outs = new FileOutputStream(new File(outputFileName));
	          ImageIO.write(image, "png", outs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void makeBinary(String inputFileName, String outputFileName){
		File img1 = new File(inputFileName);
	    BufferedImage img;
		try {
			img = ImageIO.read(img1);
		    int[] colors = new int[img.getWidth() * img.getHeight()];
		    img.getRGB(0, 0, img.getWidth(), img.getHeight(), colors, 0, img.getWidth());
		    
	//	    rgb values to image
		    BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_GRAY); 
		       for (int x = 0; x < img.getWidth() * img.getHeight(); x++) {
		          int rgb = colors[x];
		          int rownum = (int) Math.floor(x/img.getWidth());
		          int colnum = x-(rownum*img.getWidth());
		          if(rgb < -1001000) rgb = -10000000;
		          else rgb = -1;
		          image.setRGB(colnum, rownum, rgb);
		       }
		      FileOutputStream outs = new FileOutputStream(new File(outputFileName));
	          ImageIO.write(image, "png", outs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<int[]> findElements(String inputFileName, String outputFileName) {
		File img1 = new File(inputFileName);
	    BufferedImage img;
	    
		try {
			img = ImageIO.read(img1);
		    int[] colors = new int[img.getWidth() * img.getHeight()];
		    img.getRGB(0, 0, img.getWidth(), img.getHeight(), colors, 0, img.getWidth());
		    boolean[] visited = new boolean[img.getWidth() * img.getHeight()];
		    ArrayList<ArrayList<Integer>> elements = new ArrayList<>();
		    ArrayList<int[]> currentElement = new ArrayList<int[]>();
		    ArrayList<Integer> currentElementSimple = new ArrayList<Integer>();
		    int[] currentPixel = new int[2];
	//	    rgb values to image
		    BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB); 
		       for (int x = 0; x < img.getWidth() * img.getHeight(); x++) {
//		    	   //System.out.println(x + "\t" + "/" +"\t\t"+ colors.length );
	 		          int rownum = (int) Math.floor(x/img.getWidth()); 	// Y
			          int colnum = x-(rownum*img.getWidth());			// X
			          int rgb = colors[x];
		    	   image.setRGB(colnum, rownum, rgb);
		    	   ArrayList<Integer> discovered = new ArrayList<Integer>();
		    	   if (!visited[x]){
		    		   visited[x] = true;
		    		   rgb = colors[x];
		    		   if(rgb != -1/* && !visited[x]*/){
		 		          rownum = (int) Math.floor(x/img.getWidth()); 	// Y
				          colnum = x-(rownum*img.getWidth());			// X
				          currentPixel[0] = colnum;
				          currentPixel[1] = rownum;
//				          currentElement.add(currentPixel);
//				          DFSOverElement(visited, x, discovered, colors, img);
				          DFSOverElementIterative(visited, x, discovered, colors, img);
				          int[] toArray = new int[discovered.size()];
				          for(int index = 0; index < discovered.size(); index++){
				        	  toArray[index] = discovered.get(index);
				          }
				          currentElement.add(toArray);
//				          currentElementSimple.addAll(discovered);				          if(x > 500){
//				          break;
//				          DFS over found element with 8-connectivity
		    		   }
		    	   }
		       }
//		       //System.out.println("Pixelstocolor: " + currentElementSimple.size());
		       int colorOffset = 0;
		       for(int[] outer: currentElement){
		    	   colorOffset += 5000;
		    	   for(int inner: outer){
		    		   image.setRGB(inner-(((int) Math.floor(inner/img.getWidth()))*img.getWidth()), (int) Math.floor(inner/img.getWidth()), 500+colorOffset);
		    	   }
		       }
		      FileOutputStream outs = new FileOutputStream(new File(outputFileName));
	          ImageIO.write(image, "png", outs);
	          return currentElement;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("Returning null!!!!!");
		return null;
	}
	
	public void DFSOverElement(boolean[] visited, int currentPixel, ArrayList<Integer> discoveredPixels, int[] input, BufferedImage img){
		visited[currentPixel] = true;
		discoveredPixels.add(currentPixel);
		////System.out.println("Starting DFS from: " + currentPixel);
		int rownum, colnum, checkrownum, checkcolnum, checkPixel;
		rownum = (int) Math.floor(currentPixel/img.getWidth()); 	// Y
        colnum = currentPixel-(rownum*img.getWidth());				// X
        ////System.out.println("Current: " + rownum + ", " + colnum);
        for(int rowOffset = -1; rowOffset < 2; rowOffset++){
    		checkrownum = rownum + rowOffset;
    		if(checkrownum > -1){
	        	for(int colOffset = -1; colOffset < 2; colOffset++){
	        		checkcolnum = colnum + colOffset;
	        		////System.out.print("Checking: " + checkrownum);
	        		////System.out.print(", " + checkcolnum + "\n");
		        		if(checkcolnum > -1){
			                checkPixel = checkrownum * img.getWidth() + checkcolnum;
			                if(checkPixel < input.length && !visited[checkPixel]){
			                	if(input[checkPixel] == -1) visited[checkPixel] = true;
			                	else {
				                		if (!discoveredPixels.contains(checkPixel)){
				                		discoveredPixels.add(checkPixel);
//				                		////System.out.println("Discovered: " + checkPixel);
				//                		Recurse
				                		DFSOverElement(visited, checkPixel, discoveredPixels, input, img);
			                		}
			                	}
			
			                }
		        		}
	        	}
        }
        }

	}
	
	public void DFSOverElementIterative(boolean[] visited, int currentPixel, ArrayList<Integer> discoveredPixels, int[] input, BufferedImage img){
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(currentPixel);
		while(!stack.isEmpty()){
			int rownum, colnum, checkrownum, checkcolnum, checkPixel;
			currentPixel = stack.pop();
			visited[currentPixel] = true;
			rownum = (int) Math.floor(currentPixel/img.getWidth()); 		// Y
	        colnum = currentPixel-(rownum*img.getWidth());				// X
	        for(int rowOffset = -1; rowOffset < 2; rowOffset++){
	    		checkrownum = rownum + rowOffset;
	    		if(checkrownum > -1){
		        	for(int colOffset = -1; colOffset < 2; colOffset++){
		        		checkcolnum = colnum + colOffset;
		        		if(checkcolnum > -1){
		        			checkPixel = checkrownum * img.getWidth() + checkcolnum;
			                if(checkPixel < input.length && !visited[checkPixel]){
			                	if(input[checkPixel] == -1) visited[checkPixel] = true;
			                	else {
				                		if (!discoveredPixels.contains(checkPixel)){
//				                			//System.out.println("stacksize: " + stack.size());
				                			discoveredPixels.add(checkPixel);
				                			if(!stack.contains(checkPixel))
				                				stack.push(checkPixel);
			                		}
			                	}
			                }
		        		}
		        	}
	    		}
	        }
		}
	}
	
	public int[][] pixelsToObject(int[] currentElement, BufferedImage img){
		//System.out.println("Creating new object");
//		Array of pixel indexes -> boolean[][] 
		int width = img.getWidth();
		int maxX = 0, maxY = 0;
		int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
		int rownum, colnum;
//		Find min and max x and y
		if(currentElement.length > 0){
			//System.out.println(currentElement.length);
			for(int index: currentElement){
				rownum = (int) Math.floor(index/img.getWidth()); 		// Y
		        colnum = index-(rownum*img.getWidth());					// X
				if(colnum < minX) minX = colnum;
				if(rownum < minY) minY = rownum;
				if(colnum > maxX) maxX = colnum;
				if(rownum > maxY) maxY = rownum;
			}
		
			//System.out.println("" + maxY +" "+ minY + "\t" + maxX + " " + minX);
			int[][] object = new int[maxY-minY+1][maxX-minX+1];
	//		Populate obect
			for(int index: currentElement){
				rownum = (int) Math.floor(index/img.getWidth()) /*- minY*/; 		// Y
		        colnum = index-(rownum*img.getWidth()) - minX;						// X
		        rownum -= minY;
	//	        //System.out.println("x: " + colnum);
	//	        //System.out.println("minX: " + minX);
	//	        //System.out.println("diff: " + (colnum-minX));
	//	        //System.out.println("Width: " + object[0].length);
		        object[rownum][colnum] = 1;
			}
			return object;
		}
		return null;
	}
	
	public int differenceBetweenObjects(int[][] obj1, int[][] obj2){
		int differenceScore = 0;
		//System.out.println(obj1);
		//System.out.println(obj2);
//		obj2 or obj1 are nullpointers at weird cases...
		int smallerX = Math.min(obj1[0].length, obj2[0].length);
		int smallerY = Math.min(obj1.length, obj2.length);
		for(int row = 0; row < smallerY; row++) {
			for(int col = 0; col < smallerX; col++){
				if(obj1[row][col] != obj2[row][col]) differenceScore++;
			}
//			Add size differences to differenceScore
			differenceScore += Math.abs(obj1.length - obj2.length);
			differenceScore += Math.abs(obj1[0].length - obj2[0].length);
		}
		return differenceScore;
	}
	
	public int[][] getDifferenceTable(ArrayList<int[]> allElements, String fileName){
		 File img1 = new File(fileName);
		 BufferedImage img;
		try {
			img = ImageIO.read(img1);
		int[][] differenceTable = new int[allElements.size()][allElements.size()];
		System.out.println("LENGTH OF ALLELEMENTS: " + allElements.size());
		for (int row = 0; row < allElements.size(); row++){
			for(int col = 0; col < allElements.size(); col++){
				if(row != col){
					if(allElements.get(col).length > 0 && allElements.get(row).length > 0)
//						Weird nullpointer calling this
					differenceTable[row][col] = differenceBetweenObjects(pixelsToObject(allElements.get(row), img), pixelsToObject(allElements.get(col),img));
				}
				else{
					differenceTable[row][col] = 0;
				}
			}
		}
		return differenceTable;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public int[][] enlargeObject(int[][] object, int sizefactor){
		int newHeight = object.length * sizefactor;
		int newWidth = object[0].length * sizefactor;
		int[][] largerObject = new int[newHeight][newWidth];
		for(int row = 0; row < largerObject.length; row++){
			for(int col = 0; col < largerObject[0].length; col++){
				largerObject[row][col] = object[row/sizefactor][col/sizefactor];
			}
		}
		return largerObject;
	}
	
	public int[][] skeletonizeObject(int[][] object){
//		TODO: loop over enlarged object squares, creating a distancemap from edges (squares with neighbouring (4-connectivity)
//		empty square have distance 0, full squares have min neighbour +1)
		int[][]distanceMap = new int[object.length][object[0].length];
		for(int row = 0; row < object.length; row++){
			for(int col = 0; col < object[0].length; col++){
//				Find all edges and give distances of 0 (1s with neighbour 0)
				if(object[row][col] == 1){
					distanceMap[row][col] = -2;
					Stack<int[]> neighboursToCheck = new Stack();
					int[] n1 = {row - 1, col};
					int[] n2 = {row, col - 1};
					int[] n3 = {row, col + 1};
					int[] n4 = {row + 1, col};
					if(row > 0)
						neighboursToCheck.push(n1);	
					if(row < object.length - 1)
						neighboursToCheck.push(n4);
					if(col > 0)
						neighboursToCheck.push(n2);
					if(col < object[0].length - 1)
						neighboursToCheck.push(n3);
//					if any neighbor is 0, give value of 0
					innerWhileLoop:
					while(!neighboursToCheck.isEmpty()){
						int[] checking = neighboursToCheck.pop();
//						System.out.println(checking[0] + ", " + checking[1]);
//						System.out.println(object.length + "; " + object[0].length);
						if(object[checking[0]][checking[1]] == 0){
							distanceMap[row][col] = 0;
							break innerWhileLoop;
						}
					}
				}
				else{
					distanceMap[row][col] = -1;
				}
//				Check 4-connected neighbours for contents etc. and make distancemap
				
			}
		}
//		return distanceMap;
		for(int row = 0; row < object.length; row++){
			for(int col = 0; col < object[0].length; col++){
//				Find all edges and give distances of 0 (1s with neighbour 0)
				if(distanceMap[row][col] != -1 && distanceMap[row][col] != 0){
					Stack<int[]> neighboursToCheck = new Stack();
					ArrayList<Integer> neighbourValues = new ArrayList();
					int[] n1 = {row - 1, col};
					int[] n2 = {row, col - 1};
					int[] n3 = {row, col + 1};
					int[] n4 = {row + 1, col};
					if(row > 0)
						neighboursToCheck.push(n1);	
					if(row < object.length - 1)
						neighboursToCheck.push(n4);
					if(col > 0)
						neighboursToCheck.push(n2);
					if(col < object[0].length - 1)
						neighboursToCheck.push(n3);
//					if any neighbor is 0, give value of 0
					while(!neighboursToCheck.isEmpty()){
						int[] checking = neighboursToCheck.pop();
						neighbourValues.add(object[checking[0]][checking[1]]);
					}
//					Find min neighbourvalue that is not -1
					int currentMin = Integer.MAX_VALUE;
					for(int value: neighbourValues){
						if (value < currentMin && value != -1)
							currentMin = value;
					}
					distanceMap[row][col] = currentMin + 1;
				}
			}
		}
//		START SKELETONIZATION PROGRESS
//		return distanceMap;
//		loop over rows and columns, when finds 0 distance px 
//		checks off to which 4 sides, there are neighbours
//		moves toward neighbours, finding middle square (= 1 the rest = -1)
		int[][] skeleton = new int[distanceMap.length][distanceMap[0].length];
		for(int row = 0; row < distanceMap.length; row++){
			for(int col = 0; col < distanceMap[0].length; col++){
				if(distanceMap[row][col] == 0){
//					Check which neighbours are  != -1 
//					check only down and right
//					if down != -1, also check that up == -1
					if(row +1 < distanceMap.length 
							&& distanceMap[row+1][col] != -1
							&& (row-1 < 0 || distanceMap[row-1][col] == -1)){
//						Check down here
						checkDown:
						for(int checkRow = row; checkRow < distanceMap.length; checkRow++){
							if(distanceMap[checkRow][col] == -1){
//								once reaches end, fill middle (if even fill 2 sqrs)
//								checkrow - 1 / 2 if integer then fill middle, else fill 2 middles
								float end = (checkRow - row) /2;
								if(end % 1 != 0){
//									Fill 2 sqrs
									skeleton[(int) (row+Math.floor(end))][col] = 1;
									skeleton[(int) (row+Math.ceil(end))][col] = 1;
									break checkDown;
								}
								else{
//									fill 1 sqr
									skeleton[(int) (row+end)][col] = 1;
									break checkDown;
								}
							}
						}
					}
					if(col +1 < distanceMap[0].length
							&& distanceMap[row][col+1] != -1
							&& (col-1 < 0 || distanceMap[row][col-1] == -1)){
//						check right here
						checkRight:
						for(int checkCol = col; checkCol < distanceMap[0].length; checkCol++){
							if(distanceMap[row][checkCol] == -1){
								float end = (checkCol - col) / 2;
								if(end % 1 != 0){
//									fill 2 sqrs
									skeleton[row][(int) (col + Math.floor(end))] = 1;
									skeleton[row][(int) (col + Math.ceil(end))] = 1;
									break checkRight;
								}
								else{
//									fill 1 sqr
									skeleton[(int) (row)][(int) (col + end)] = 1;
									break checkRight;
								}
							}
						}
					}
				}
			}
		}
		return skeleton;
	}
	
	public ArrayList<int[]> orderObjects(ArrayList<int[]> allElements, int[][] differenceTable){
		int[] currentMin = {0,0};
		outerloop:
		for(int outer = 0; outer < differenceTable.length; outer++){
			for(int inner = 0; inner < differenceTable[0].length; inner++){
				if(outer != inner){
					if(differenceTable[outer][inner] == 0){
						currentMin[0] = outer;
						currentMin[1] = inner;
						break outerloop;
					}
					else if(differenceTable[outer][inner] < differenceTable[currentMin[0]][currentMin[1]]){
						currentMin[0] = outer;
						currentMin[1] = inner;
					}
				}
			}
		}
//		Here we have the smallest possible difference between any two objects
//		Have to order the elements in allElements based on the differences in differenceTable[currentMin[0]]
		ArrayList<int[]> orderedObjects = new ArrayList<int[]>();
//		Add first element to orderedObjects
		orderedObjects.add(allElements.get(currentMin[1]));
//		Stack<Integer> row = new Stack();
		int[] row = differenceTable[currentMin[0]];
//		While not every checked
		while(orderedObjects.size() < row.length){
			int minAtIndex = 0;
			int minDiff = Integer.MAX_VALUE;
			for(int index = 0; index < row.length; index ++){
//				System.out.println(index + ". " + row[index] + " < " + minDiff + " ?");
				if (row[index] < minDiff && index != currentMin[0]){
//					System.out.println("So changing");
					minDiff = row[index];
					minAtIndex = index;
				}
				if(minDiff == 0){
					break;
				}
//				System.out.println("At index: " + index + " value is: " + row[index]);
			}
			row[minAtIndex] = Integer.MAX_VALUE;
			orderedObjects.add(allElements.get(minAtIndex));
		}
		return orderedObjects;
	}
	
	public void objectsIntoOutput(ArrayList<int[]> objects, String inputFileName, String outputFileName){
//		Get max width of all objects
//		get total height of all objects
		File img1 = new File(inputFileName);
	    BufferedImage img;
	    try {
			img = ImageIO.read(img1);
	    int maxWidth = 0;
	    int sumHeight = 0;
	    int[] startPoints = new int[objects.size()];
	    ArrayList<int[][]> parsedObjects = new ArrayList();
	    for(int index = 0; index < objects.size(); index++ /*int[] pixels: objects*/){
	    	if(objects.get(index).length > 0){
		    	int[][] object = pixelsToObject(objects.get(index), img);
		    	parsedObjects.add(object);
		    	System.out.println(object.length);
		    	if (object[0].length > maxWidth)
		    		maxWidth = object[0].length;
		    	startPoints[index] = sumHeight;
		    	sumHeight += object.length + 1; //adding separator space 
		    }
	    }
	    
		BufferedImage image = new BufferedImage(maxWidth, sumHeight, BufferedImage.TYPE_INT_RGB);
//		go over all objects again and transpose them onto the new image resolutions.
		for(int index = 0; index < parsedObjects.size()/*startPoints.length*/; index++){
//			Might have to fill areas inbetween 
			for(int rownum = 0; rownum < parsedObjects.get(index).length; rownum++/*int[] row: parsedObjects.get(index)*/){
				for(int colnum = 0; colnum < parsedObjects.get(index)[rownum].length; colnum++/*int col: row*/){
					image.setRGB(colnum, rownum + startPoints[index], (parsedObjects.get(index)[rownum][colnum] == 1)? 5000000:0);
				}
			}
		}
		
		FileOutputStream outs;
			outs = new FileOutputStream(new File(outputFileName));
			ImageIO.write(image, "png", outs);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 public static void main(String... args) throws Exception {
		 main main = new main();
		 main.grayScale("compare6.png", "3.png");
////		 TODO: initial edge detection to separate overlaying symbols (of different colors for example)?
		 main.contrastIncrease("3.png", "3.png", 1.5f);
//		 main.edgeDetect("GoogleTartu.png", "3.png");
//		 main.edgeDetectSobel("UTblurred.jpeg", "3.png");
////		 TODO: experimental gradient edge detection to separate symbols? USE (non topology saving) THINNING !!!!!!!!!!!!!!
////		 TODO: directional edge detection with 8-connectivity?
////		 	TODO: directional continued edge probabilistic cutoff?
////		 TODO: experimental center of mass probabilistic edge cutoff?
		 main.makeBinary("3.png", "3.png");
		 ArrayList<int[]> allElements;
		 allElements = main.findElements("3.png", "3.png");
////		 TODO: noise removal with DM algorithms (outliers etc) ?
////		 TODO: cluster found elements / order them per a variation of Levenshtein's distance
////		 TODO: Make image / objects larger, so 1px would be 3x3, and cuttable
//		 main.pixelsToObject(allElements.get(1, img);
		 
		 int[][] differenceTable = main.getDifferenceTable(allElements, "3.png");
//		 System.out.println("Done");
//		 ArrayList<int[]> orderedElements = main.orderObjects(allElements, differenceTable);
//		 main.objectsIntoOutput(orderedElements, "3.png", "Output.PNG");
		 
		 
		 
		 //		 for(int x = 0; x < differenceTable[0].length; x++){
//			 System.out.print(x + "\t");
//		 }
		 for(int[] line: differenceTable){
			 for(int col: line){
				 System.out.print(col+ "\t");
			 }
			 System.out.println(" ");
		 }
		 
		 		 
//		 File img1 = new File("3.png");
//		 BufferedImage img = ImageIO.read(img1);
////		 BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
//		 int[][] obj = main.pixelsToObject(allElements.get(1), img);
//		 int[][] largeObj = main.enlargeObject(obj, 3);
//		 int[][] skeleton = main.skeletonizeObject(largeObj);
//		 for(int[] line: skeleton){
//			 for(int pix: line){
//				 System.out.print(pix + " ");
//			 }
//			 System.out.print("\n");
//		 }
		    }	    
	 }

