import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

// receives DMap as int[][] and creates a skeleton, sends skeleton to UI and progress to A4
public class A4 implements Runnable{

	private Thread thread;
	protected BlockingQueue<int[][]> queue;
	
	public A4() { this(new LinkedBlockingQueue<int[][]>()); }
	
	public A4(LinkedBlockingQueue<int[][]> linkedBlockingQueue) {
		this.queue = linkedBlockingQueue;
		thread = new Thread(this, this.toString());
	}
	
	private void messageOut(int[][] msg, A6 actor6){
		actor6.send(msg);
	}
	
//	TODO: implement this
	public void process(int[][] distanceMap) {
		System.out.println("Actor4 start");
		A6 actor6 = new A6();
		actor6.start();
		
		int[][] skeleton = new int[distanceMap.length][distanceMap[0].length];
		for(int row = 0; row < distanceMap.length; row++){
			for(int col = 0; col < distanceMap[0].length; col++){
				if(distanceMap[row][col] == 0){
//					Check which neighbours are  != -1 
//					check only down and right
//					if down != -1, also check that up == -1
					if(row +1 < distanceMap.length 
							&& distanceMap[row+1][col] != -1
							&& (row-1 < 0 || distanceMap[row-1][col] == -1)){
//						Check down here
						checkDown:
						for(int checkRow = row; checkRow < distanceMap.length; checkRow++){
							if(distanceMap[checkRow][col] == -1){
//								once reaches end, fill middle (if even fill 2 sqrs)
//								checkrow - 1 / 2 if integer then fill middle, else fill 2 middles
								float end = (checkRow - row) /2;
								if(end % 1 != 0){
//									Fill 2 sqrs
									skeleton[(int) (row+Math.floor(end))][col] = 1;
									skeleton[(int) (row+Math.ceil(end))][col] = 1;
									break checkDown;
								}
								else{
//									fill 1 sqr
									skeleton[(int) (row+end)][col] = 1;
									break checkDown;
								}
							}
						}
					}
					if(col +1 < distanceMap[0].length
							&& distanceMap[row][col+1] != -1
							&& (col-1 < 0 || distanceMap[row][col-1] == -1)){
//						check right here
						checkRight:
						for(int checkCol = col; checkCol < distanceMap[0].length; checkCol++){
							if(distanceMap[row][checkCol] == -1){
								float end = (checkCol - col) / 2;
								if(end % 1 != 0){
//									fill 2 sqrs
									skeleton[row][(int) (col + Math.floor(end))] = 1;
									skeleton[row][(int) (col + Math.ceil(end))] = 1;
									break checkRight;
								}
								else{
//									fill 1 sqr
									skeleton[(int) (row)][(int) (col + end)] = 1;
									break checkRight;
								}
							}
						}
					}
				}
			}
		}
//		Send skeleton message
		messageOut(skeleton, actor6);
		System.out.println("Actor4 end");
}
	
	public void start(){
		thread.start();
	}
	
	public void send(int[][] msg) {
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		try {
			while(true){
			int[][] message = queue.take();
			process(message);
//			Send messages to 2 other actors
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
