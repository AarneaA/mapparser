import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.imageio.ImageIO;

// receives image as filename and DFSes it, sends symbol by symbol to A2 and progress to A4
public class A1 implements Runnable{

	private Thread thread;
	protected BlockingQueue<String> queue;
	
	public A1() { 
		this(new LinkedBlockingQueue<String>());
		thread = new Thread(this, this.toString());
		}
	
	public A1(LinkedBlockingQueue<String> linkedBlockingQueue) {
		this.queue = linkedBlockingQueue;
		thread = new Thread(this, this.toString());
	}
	
	public void messageOut(int[] msg, A2 actor2){
		actor2.send(msg);
	}
	
	private void DFSOverElementIterative(boolean[] visited, int currentPixel, ArrayList<Integer> discoveredPixels, int[] input, BufferedImage img){
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(currentPixel);
		while(!stack.isEmpty()){
			int rownum, colnum, checkrownum, checkcolnum, checkPixel;
			currentPixel = stack.pop();
			visited[currentPixel] = true;
			rownum = (int) Math.floor(currentPixel/img.getWidth()); 		// Y
	        colnum = currentPixel-(rownum*img.getWidth());				// X
	        for(int rowOffset = -1; rowOffset < 2; rowOffset++){
	    		checkrownum = rownum + rowOffset;
	    		if(checkrownum > -1){
		        	for(int colOffset = -1; colOffset < 2; colOffset++){
		        		checkcolnum = colnum + colOffset;
		        		if(checkcolnum > -1){
		        			checkPixel = checkrownum * img.getWidth() + checkcolnum;
			                if(checkPixel < input.length && !visited[checkPixel]){
			                	if(input[checkPixel] == -1) visited[checkPixel] = true;
			                	else {
				                		if (!discoveredPixels.contains(checkPixel)){
//				                			//System.out.println("stacksize: " + stack.size());
				                			discoveredPixels.add(checkPixel);
				                			if(!stack.contains(checkPixel))
				                				stack.push(checkPixel);
			                		}
			                	}
			                }
		        		}
		        	}
	    		}
	        }
		}
	}
	
//	TODO: implement this
//	DFS over image
	public void process(String inputFileName){
		System.out.println("Actor1 start");
		
//		Start A2
		A2 actor2 = new A2();
		actor2.start();
		AUI actorUI = new AUI();
		actorUI.start();
		
		File img1 = new File(inputFileName);
	    BufferedImage img;
	    
		try {
			img = ImageIO.read(img1);
		    int[] colors = new int[img.getWidth() * img.getHeight()];
		    img.getRGB(0, 0, img.getWidth(), img.getHeight(), colors, 0, img.getWidth());
		    boolean[] visited = new boolean[img.getWidth() * img.getHeight()];
		    ArrayList<ArrayList<Integer>> elements = new ArrayList<>();
		    ArrayList<int[]> currentElement = new ArrayList<int[]>();
		    ArrayList<Integer> currentElementSimple = new ArrayList<Integer>();
		    int[] currentPixel = new int[2];
	//	    rgb values to image
		    BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB); 
		       for (int x = 0; x < img.getWidth() * img.getHeight(); x++) {
//		    	   //System.out.println(x + "\t" + "/" +"\t\t"+ colors.length );
	 		          int rownum = (int) Math.floor(x/img.getWidth()); 	// Y
			          int colnum = x-(rownum*img.getWidth());			// X
			          int rgb = colors[x];
		    	   image.setRGB(colnum, rownum, rgb);
		    	   ArrayList<Integer> discovered = new ArrayList<Integer>();
		    	   if (!visited[x]){
		    		   visited[x] = true;
		    		   rgb = colors[x];
		    		   if(rgb != -1/* && !visited[x]*/){
		 		          rownum = (int) Math.floor(x/img.getWidth()); 	// Y
				          colnum = x-(rownum*img.getWidth());			// X
				          currentPixel[0] = colnum;
				          currentPixel[1] = rownum;
//				          currentElement.add(currentPixel);
//				          DFSOverElement(visited, x, discovered, colors, img);
				          DFSOverElementIterative(visited, x, discovered, colors, img);
				          int[] toArray = new int[discovered.size()];
				          for(int index = 0; index < discovered.size(); index++){
				        	  toArray[index] = discovered.get(index);
				          }
//				          TODO: Message toArray to A2
				          messageOut(toArray, actor2);
				          actorUI.send("A1");
				          System.out.println("Actor1 end");
		    		   }
		    	   }
		       }	             
		} catch (IOException e) {
			e.printStackTrace();
		}			
	};
	
	public void start(){
		thread.start();
	}
	
	public void send(String msg) {
		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		try {
			while(true){
			String message = queue.take();
			process(message);
//			Send messages to 2 other actors
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
